SHELL := /bin/bash

# Service specific vars
export COMPONENT_NAME := go-user-api
export COMPONENT_ARGS := $(if $(COMPONENT_ARGS),$(COMPONENT_ARGS),)

export SERVICE_PORT := $(if $(SERVICE_ADDR),$(SERVICE_ADDR),8899)
export SERVICE_ADDR := $(if $(SERVICE_ADDR),$(SERVICE_ADDR),:$(SERVICE_PORT))

export GOLANG_LINTER_IMAGE :=golangci/golangci-lint:v1.27.0

export GOFLAGS:=-mod=vendor
export E2E_TESTS_BIN := $(COMPONENT_NAME)_e2e_tests
.PHONY: proto
proto:
	./pkg/proto/gen-go.sh

.PHONY: run
run: build
	docker build -t ${COMPONENT_NAME}-image .
	docker run -u 1000:1000 -p ${SERVICE_PORT}:${SERVICE_PORT} ${COMPONENT_NAME}-image

.PHONY: build
build:
	GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o ./bin/$(COMPONENT_NAME) ./cmd/$(COMPONENT_NAME)/

.PHONY: lint
lint:
	docker run --rm \
		-v ${PWD}:/go \
		${GOLANG_LINTER_IMAGE} golangci-lint run ${GOLANG_LINTER_ARGS}

.PHONY: test-all
test-all: test test-e2e

.PHONY: test
test:
	go test ./... -timeout 30s -count=1 ${ARGS}

.PHONY: test-e2e
test-e2e: build
	./e2e.sh

.PHONY: cover
cover:
	mkdir -p bin/
	mkdir -p cover/
	@echo "NOTE: make cover does not exit 1 on failure, don't use it to check for tests success!"
	rm -f cover/*.out cover/all.merged
	go test ./... -count 1 -timeout 30s -coverprofile=cover/unit.out
	go tool cover -html cover/unit.out -o cover/coverage.html
	@echo ""
	@echo "=====> Total test coverage: <====="
	@echo ""
	go tool cover -func cover/unit.out
